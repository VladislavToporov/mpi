#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>


void print(int *a, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            printf("%d, ",  a[i * n + j]);
        }
        printf("\n");
    }
    printf("\n");
}

void init(int *send_buf, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            send_buf[i * n + j] = rand() % 5;
        }
    }
    print(send_buf, n, m);
}

int main(int argc, char** argv)
{
    int size, rank; //число процессов, номер текущего процесса
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int n = 4;
    int m = 4;

    if (size < n) {
        printf("Please run with %d or more process.\n", n);
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }

    int send_count = m;
    int recv_count = send_count;

    int send_buf_1[n * m];
    int send_buf_2[n * m];

    if (rank == 0) {
        srand(time(NULL));
        init(send_buf_1, n, m);
        init(send_buf_2, n, m);
    }

    int recv_buff_1[recv_count];
    int recv_buff_2[recv_count];
    int sum_i = 0, max = 0;
    MPI_Scatter(send_buf_1, send_count, MPI_INT, recv_buff_1, recv_count, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(send_buf_2, send_count, MPI_INT, recv_buff_2, recv_count, MPI_INT, 0, MPI_COMM_WORLD);

    for (int i = 0; i < recv_count; i++) {
        recv_buff_1[i] *= recv_buff_2[i];
    }

    MPI_Gather(recv_buff_1, recv_count, MPI_INT, send_buf_1, send_count, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank == 0){
        print(send_buf_1, n, m);
    }

    MPI_Finalize();
    return 0;
}