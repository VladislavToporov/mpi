#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

//Написать программу, запустить ее на 2х процессах. На нулевом процессе задать массив а из 10 элементов,
// значения сгенерировать случайным образом. Переслать этот массив целиком первому процессу с помощью функции
// MPI_Send. Распечатать на первом процессе принятый массив.

int main(int argc, char *argv[]) {
    int size, rank;
    srand(time(NULL));
    int n = 10;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size != 2) {
        printf("Please run with two processes.\n");
        fflush(stdout);
        MPI_Finalize();
        return -1;
    }

    if (rank == 0) {
        int a[n];
        printf("Process %d \n", rank);
        for (int i = 0; i < n; i++) {
            a[i] = rand() % 1000; // размах значений случайных величин 0 - 999
            printf("%d, ", a[i]);
        }
        printf("\n");
        // Отпрвляем массив a целых чисел (int) из 10 элементов процессу 1 с label = 5, указываем общий для всех процессов коммутатор MPI_COMM_WORLD
        MPI_Send(a, 10, MPI_INT, 1, 5, MPI_COMM_WORLD);
    } else if (rank == 1) {
        int a[n];
        // Получаем массив a целых чисел (int) из 10 элементов процессу 1 с label = 5, статус сообщения,
        // указываем общий для всех процессов коммутатор MPI_COMM_WORLD
        MPI_Recv(a, 10, MPI_INT, 0, 5, MPI_COMM_WORLD, &status);
        printf("Process %d \n", rank);
        for (int i = 0; i < n; i++) {
            printf("%d, ", a[i]);
        }
        printf("\n");

    }
    MPI_Finalize();
    return 0;
}