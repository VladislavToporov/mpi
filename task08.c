#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

void printMatrix(int *a, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            printf("%d, ", a[i * n + j]);
            fflush(stdout);
        }
        printf("\n");
        fflush(stdout);
    }
    printf("\n");
    fflush(stdout);
}


void initRow(int *a, int *row, int n, int i) {
    for (int j = 0; j < n; j++) {
        row[j] = a[i * n + j];
    }
}

void initColumn(int *a, int *column, int n, int i) {
    for (int j = 0; j < n; j++) {
        column[j] = a[j * n + i];
    }
}

void collect(int n, int size) {
    MPI_Status status;
    int z[n * n];
    for (int i = 0; i < size; i++) {
        int *z_substring = (int *) malloc(sizeof(int) * n);
        MPI_Recv(z_substring, n, MPI_INT, i, i,
                 MPI_COMM_WORLD, &status);
//        printf("%d received %d numbers from %d \n", 0, n, i);
        for (int j = 0; j < n; j++) {
            z[i * n + j] = z_substring[j];
        }
    }
    printMatrix(z, n);
}


int mult(int x, int y) {
    return x * y;
}

void sendInitialRowAndColumn(int *x, int *y, int n, int i) {
    int row[n];
    initRow(x, row, n, i);
    MPI_Send(row, n, MPI_INT, i, i, MPI_COMM_WORLD); // отправляем i-ый ряд i процессу с меткой i
//    printf("0 sent %d numbers to %d \n", n, i);
//    fflush(stdout);
//    MPI_Send(&i, 1, MPI_INT, i, 0, MPI_COMM_WORLD); // отправляем i столбца i процессу с меткой 0
//    printf("0 sent %d to %d \n", i, i);
//    fflush(stdout);
    for (int j = 0; j < n; j++) {
        int column[n];
        initColumn(y, column, n, j);
        MPI_Send(column, n, MPI_INT, i, i, MPI_COMM_WORLD); // отправляем i-ый столбец i процессу с меткой i
    }
//    printf("0 sent %d numbers to %d \n", n, i);
//    fflush(stdout);
}

void mult2Calculate(int n, int rank) {
    MPI_Status status;
    int z_substring[n];
    int *row = (int *) malloc(sizeof(int) * n);
    MPI_Recv(row, n, MPI_INT, 0, rank,
             MPI_COMM_WORLD, &status); // принимаем i-ый ряд на i процессе от 0 процесса с меткой i
//    printf("%d received %d numbers from %d \n", rank, n, 0);
//    fflush(stdout);

    for (int i = 0; i < n; i++) {
        int *column = (int *) malloc(sizeof(int) * n);

        MPI_Recv(column, n, MPI_INT, 0, rank,
                 MPI_COMM_WORLD, &status); // принимаем i-ый столбец на i процессе от 0 процесса с меткой i
//        printf("%d received %d numbers from %d \n", rank, n, 0);
//        fflush(stdout);

        z_substring[i] = 0;
        for (int j = 0; j < n; j++) {
            z_substring[i] += row[j] * column[j];
            //printf("z_substring [%d] = %d;\n", rank, z_substring[column_idx]);
        }
        //printf("\n");

    }
    MPI_Send(z_substring, n, MPI_INT, 0, rank, MPI_COMM_WORLD);
//    printf("%d sent %d numbers to %d \n", rank, n, 0);
//    fflush(stdout);
}

int main(int argc, char *argv[]) {
    int size, rank;
    int n = 4, mode = 1;

    MPI_Status status;
    // инициализруем параллельную область
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size != n) {
        printf("Please run with %d processes.\n", n);
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }

    if (rank == 0) {
        srand(time(NULL));
        int x[n * n], y[n * n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                x[i * n + j] = rand() % 5;
                y[i * n + j] = rand() % 5;
            }
        }
        printMatrix(x, n);
        printMatrix(y, n);

        for (int i = 0; i < size; i++) {
           sendInitialRowAndColumn(x, y, n, i);
        }
    }

    if (mode == 1) {
        mult2Calculate(n, rank);
    }

    if (rank == 0) {
        collect(n, size);
    }

    // закрываем параллельную область
    MPI_Finalize();
    return 0;
}