#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>
int main( int argc, char ** argv )
{
    int vector[4][4] = { 5, 5, 5, 5, 5, 4, 4, 5, 5, 4, 4, 5, 5, 5, 5, 5 };
    int nvector[2][2];
    int blocklengths = 2;
    int stride = 4;
    int rank;
    MPI_Datatype mytype;
    MPI_Datatype wtype;
    MPI_Status mystatus;
    int i, j;

    MPI_Init( &argc, &argv );

    MPI_Type_vector( 2, 2, 4, MPI_INT, &mytype );
    MPI_Type_vector( 2, 2, 2, MPI_INT, &wtype );

    MPI_Type_commit( &mytype );
    MPI_Type_commit( &wtype );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    if ( rank == 0 )
    {
        MPI_Send( &vector[1][1], 1, mytype, 1, 0, MPI_COMM_WORLD );
    }
    else
    {
        for (i = 0; i < 4; i++)
        {
            printf("\n" );
            for (j=0; j < 4; j++)
                printf("%i ", nvector[i][j]);
        }

        printf("\n" );
        MPI_Recv( &nvector[0][0], 1, wtype, 0, 0, MPI_COMM_WORLD, &mystatus );
        printf("\n" );
        for (i = 0; i < 2; i++)
        {
            printf("\n" );
            for (j=0; j < 2; j++)
                printf("%i ", nvector[i][j] );
        }
        printf("\n" );
    }

    MPI_Finalize( );
    return 0;
}