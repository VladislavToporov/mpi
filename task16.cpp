#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>

int main(int argc, char **argv) {
    int rank, size;
    int ranks[5] = {8, 3, 9, 1, 6};

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Group wgroup, group1;
    MPI_Comm_group(MPI_COMM_WORLD, &wgroup);
    MPI_Group_incl(wgroup, 5, ranks, &group1);

    MPI_Comm comm1, icomm, local;
    MPI_Comm_create(MPI_COMM_WORLD, group1, &comm1);

    double a[3];
    if (comm1 != MPI_COMM_NULL) {
        int rank1 = -1, size1 = -1;
        MPI_Comm_rank(comm1, &rank1);
        MPI_Comm_size(comm1, &size1);

        if (rank1 == 0) {
            for (int i = 0; i < 3; i++) {
                a[i] = i;
                printf("%f, ", a[i]);
            }
            printf("\n");
        }
        MPI_Bcast(a, 3, MPI_DOUBLE, 0, comm1);
        printf("%d: \n", rank);
        for (int i = 0; i < 3; i++) {
            printf("%f, ", a[i]);
        }
        printf("\n");
        if (rank == size1 - 1) {
            MPI_Send(a, 3, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
        }
    }

    if (MPI_COMM_WORLD != MPI_COMM_NULL && rank == 0) {
        //MPI_Recv(a, 3, MPI_DOUBLE, MPI_ANY_SOURCE, 0, comm1, MPI_STATUSES_IGNORE);
    }

    if (comm1 != MPI_COMM_NULL)
        MPI_Comm_free(&comm1);
    MPI_Group_free(&group1);
    MPI_Finalize();
}
