#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

//Используя блокирующую операцию передачи сообщений (MPI_Send и MPI_Recv) выполнить пересылку данных
// одномерного массива, из процесса с номером 1 на остальные процессы группы. На процессах получателях
// до выставления функции приема (MPI_Recv) определить количество принятых данных (MPI_Probe).
// Выделить память под размер приемного буфера, после чего вызвать функцию MPI_Recv.
// Полученное сообщение выдать на экран.

int main(int argc, char *argv[]) {
    int size, rank;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size < 2) {
        printf("Please run with two and more processes.\n");
        fflush(stdout);
        MPI_Finalize();
        return -1;
    }

    int number_amount;
    if (rank == 1) {
        int number_amount = 10;
        int array[number_amount];
        srand(time(NULL));
        for (int i = 0; i < size; i++) {
            if (i == 1) {
                continue;
            }
            MPI_Send(array, number_amount, MPI_INT, i, 0, MPI_COMM_WORLD);
            printf("1 sent %d numbers to %d\n", number_amount, i);
        }
    }
    if (rank > 1 || rank == 0) {
        MPI_Status status;
        MPI_Probe(1, 0, MPI_COMM_WORLD, &status);

        //Получам статус и аттрибуты сообщения (number_amount)
        MPI_Get_count(&status, MPI_INT, &number_amount);

        // Создаем буфер
        int* array_buf = (int*)malloc(sizeof(int) * number_amount);

        // Получаем сообщение, содержащее массив данных, записываем в буфер
        MPI_Recv(array_buf, number_amount, MPI_INT, 1, 0,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("%d received %d numbers from 0.\n",
               rank, number_amount);
        // Освобождаем память
        free(array_buf);
    }
    MPI_Finalize();
    return 0;
}