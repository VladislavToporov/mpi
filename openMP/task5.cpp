#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
// Написать программу, в которой объявить и присвоить начальные значения элементам
//двумерного массива d[6][8], для инициализации значений использовать генератор случайных
//чисел. Используя конструкцию директивы sections...section определить три секции для
//выполнения следующих операций:
//первая секция выполняет вычисление среднего арифметического значения элементов
//двумерного массива,
//вторая секция выполняет вычисление минимального и максимального значений
//элементов двумерного массива,
//третья секция выполняет вычисление количества элементов массива, числовые значения
//которых кратны 3.
//В каждой секции определить и выдать на экран номер исполняющей нити и результат
//выполнения вычислений.

int main() {
    int n = 6;
    int m = 8;
    int d[n][m];
    srand(time(NULL));

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            d[i][j] = rand() % 1000; // размах значений случайных величин 0 - 1000
        }
    }

//    for (int i = 0; i < n; i++) {
//        for (int j = 0; j < m; j++) {
//            printf("%d, ", d[i][j]);
//        }
//    }
//    printf("\n");


    #pragma omp parallel sections
    {
        #pragma omp section
        {
            int sum = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    sum += d[i][j];
                }
            }
            printf("Thread id = %d; average value = %d\n", omp_get_thread_num(), sum / (n * m));
        }
        #pragma omp section
        {
            int min = d[0][0];
            int max = min;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (d[i][j] < min)
                        min = d[i][j];
                    if (d[i][j] > max)
                        max = d[i][j];
                }
            }
            printf("Thread id = %d; min = %d; max = %d\n", omp_get_thread_num(), min, max);
        }

        #pragma omp section
        {
            int counter = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (d[i][j] % 3 == 0)
                        counter++;
                }
            }
            printf("Thread id = %d; counter_3 = %d\n", omp_get_thread_num(), counter);
        }
        // так как по умолчанию на каждую секцию мой компилятор выделяет по единственному потоку,
        // pragma omp atomic в программе не используется
    }

    return 0;
}

//output:
//Thread id = 2; average value = 496
//Thread id = 1; min = 0; max = 972
//Thread id = 0; counter_3 = 10