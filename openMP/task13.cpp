#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
//#include <cmath>
//На нулевом процессе задан массив целых чисел, который является двоичным представлением
//десятичного числа. Написать параллельную программу вычисления десятичного числа из
//заданного двоичного представления.

int pow(int base, int deg) {
    int res = 1;
    for(int i = 0; i < deg; i++) {
        res *= base;
    }
    return res;
}
int main() {
    int n = 30;
    int a[] = {1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1};
    int sum = 0;
    omp_set_num_threads(8);
    #pragma omp parallel for reduction(+:sum)
    // reduction аккумулирует частные суммы, а затем сливает эти значение в одно в главном потоке
    for (int i = n - 1; i >= 0; i--) {
        if (a[i] == 1) {
            sum += pow(2, n - i - 1);
        }
    }
    printf("number = %d;\n", sum);


    return 0;
}

//output:
//number = 756893125;
