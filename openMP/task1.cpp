#include <stdio.h>
#include <omp.h>

#define THREADS_NUMBER 8

// Написать программу где каждый поток печатает свой идентификатор, количество потоков
//всего и строчку «Hello World». Запустить программу с 8 потоками. Всегда ли вывод
//идентичен? Почему?

int main()
{
#pragma omp parallel num_threads(THREADS_NUMBER)
    {
        printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
    }
    // вывод не всегда идентичен, так как печать в консоль выполняется несколькими потоками параллельно,
    // управление переходит от потока к потоку в случайном порядке
    return 0;
}


// output:
// Thread id = 3; Number od threads = 8; Hello World
//Thread id = 4; Number od threads = 8; Hello World
//Thread id = 1; Number od threads = 8; Hello World
//Thread id = 2; Number od threads = 8; Hello World
//Thread id = 5; Number od threads = 8; Hello World
//Thread id = 6; Number od threads = 8; Hello World
//Thread id = 0; Number od threads = 8; Hello World
//Thread id = 7; Number od threads = 8; Hello World
