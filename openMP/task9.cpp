#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
//Написать программу, в которой объявить и присвоить начальные значения элементам
//двумерного массива d[6][8], для инициализации значений использовать генератор случайных
//чисел. Используя конструкцию директивы omp parallel for и omp critical определить
//минимальное и максимальное значения элементов двумерного массива. Количество нитей
//задать самостоятельно. Результат выдать на экран.

int main() {
    int n = 6;
    int m = 8;
    int d[n][m];

    srand(time(NULL));

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            d[i][j] = rand() % 1000; // размах значений случайных величин 0 - 1000
        }
    }

    int min = d[0][0];
    int max = min;

    omp_set_num_threads(8);
    #pragma omp parallel for collapse(2) // распределение итераций будет происходить
    // с учетом глубины вложенности цикла = 2
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (d[i][j] < min) {
                #pragma omp critical
                if (d[i][j] < min) {
                    // этот блок будет выполнятся только одним потоком в конкретный момент времени
                    min = d[i][j];
                }

            }
            if (d[i][j] > max) {
                #pragma omp critical
                max = d[i][j];
            }
        }
    }
    printf("min = %d; max = %d", min, max);


    return 0;
}

//output:
//min = 1; max = 946