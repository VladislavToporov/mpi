#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
// Написать программу, в которой определить две параллельные области, каждая из которых
//содержит итерационную конструкцию for выполняющую инициализацию элементов
//одномерных массивов целых чисел a[12], b[12] и c[12]. Число нитей перед первой областью
//задать равным 3, перед второй – равным 4. Первая параллельная область выполняет
//инициализацию элементов массивов a и b с использованием статического распределения
//итераций, размер порции итераций выбрать самостоятельно, вторая параллельная область
//выполняет инициализацию элементов массива c по следующему правилу c[i] = a[i] + b[i], с
//использованием динамического распределения итераций, размер порции итераций выбрать
//самостоятельно. В каждой области определить и выдать на экран количество нитей, номер
//нити и результат выполнения цикла. Убедиться в правильности работы программы.

int main() {
    int n = 12;
    int a[n], b[n], c[n];

    #pragma omp parallel num_threads(3)
    {
        #pragma omp for schedule(static, 2)
        // итерации будут распределены на порции по 2. Каждому потоку выделяется по порции
        for (int i = 0; i < n; i++) {
            a[i] = i;
            b[i] = n - i;
            printf("aThread id = %d; a[%d] = %d; Number of threads = %d;\n",
                   omp_get_thread_num(), i, a[i], omp_get_num_threads());
            printf("bThread id = %d; b[%d] = %d; Number of threads = %d;\n",
                   omp_get_thread_num(), i, b[i], omp_get_num_threads());
        }
    }

#pragma omp parallel num_threads(4)
    {
        #pragma omp for schedule(dynamic, 2)
        // итерации будут распределены на порции по 2. Каждому потоку выделится новая порция, когда он закончит работу
        for (int i = 0; i < n; i++) {
            c[i] = a[i] + b[i];
            printf("cThread id = %d; c[%d] = %d; Number of threads = %d;\n",
                   omp_get_thread_num(), i, c[i], omp_get_num_threads());
        }
    }

    return 0;
}

// output:
//aThread id = 1; a[4] = 4; Number of threads = 3;
//aThread id = 2; a[8] = 8; Number of threads = 3;
//aThread id = 0; a[0] = 0; Number of threads = 3;
//bThread id = 1; b[4] = 8; Number of threads = 3;
//bThread id = 2; b[8] = 4; Number of threads = 3;
//bThread id = 0; b[0] = 12; Number of threads = 3;
//aThread id = 1; a[5] = 5; Number of threads = 3;
//aThread id = 2; a[9] = 9; Number of threads = 3;
//aThread id = 0; a[1] = 1; Number of threads = 3;
//bThread id = 1; b[5] = 7; Number of threads = 3;
//bThread id = 2; b[9] = 3; Number of threads = 3;
//bThread id = 0; b[1] = 11; Number of threads = 3;
//aThread id = 1; a[6] = 6; Number of threads = 3;
//aThread id = 2; a[10] = 10; Number of threads = 3;
//aThread id = 0; a[2] = 2; Number of threads = 3;
//bThread id = 1; b[6] = 6; Number of threads = 3;
//bThread id = 2; b[10] = 2; Number of threads = 3;
//bThread id = 0; b[2] = 10; Number of threads = 3;
//aThread id = 1; a[7] = 7; Number of threads = 3;
//aThread id = 2; a[11] = 11; Number of threads = 3;
//aThread id = 0; a[3] = 3; Number of threads = 3;
//bThread id = 1; b[7] = 5; Number of threads = 3;
//bThread id = 2; b[11] = 1; Number of threads = 3;
//bThread id = 0; b[3] = 9; Number of threads = 3;
//cThread id = 1; c[0] = 12; Number of threads = 4;
//cThread id = 0; c[2] = 12; Number of threads = 4;
//cThread id = 2; c[4] = 12; Number of threads = 4;
//cThread id = 1; c[1] = 12; Number of threads = 4;
//cThread id = 3; c[6] = 12; Number of threads = 4;
//cThread id = 0; c[3] = 12; Number of threads = 4;
//cThread id = 2; c[5] = 12; Number of threads = 4;
//cThread id = 1; c[8] = 12; Number of threads = 4;
//cThread id = 3; c[7] = 12; Number of threads = 4;
//cThread id = 0; c[10] = 12; Number of threads = 4;
//cThread id = 1; c[9] = 12; Number of threads = 4;
//cThread id = 0; c[11] = 12; Number of threads = 4;