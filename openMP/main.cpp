#include <iostream>
#include <stdio.h>
#include <omp.h>
using namespace std;

#define THREADS_NUMBER 8
//int main() {
//    cout << "Hello, World!" << endl;
//    return 0;
//}


int main()
{
#pragma omp parallel num_threads(THREADS_NUMBER)
    {
        printf("id = %d; Threads number = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
    }
    return 0;
}