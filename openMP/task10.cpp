#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
//Написать программу, в которой объявить и присвоить начальные значения массиву целых
//чисел a[30], для инициализации значений использовать генератор случайных чисел.
//Используя конструкцию omp parallel for и omp atomic вычислить количество элементов
//массива, числовые значения которых кратны 9. Количество нитей задать самостоятельно.
//Результат выдать на экран.

int main() {
    int n = 30;
    int a[n];

    srand(time(NULL));

    for (int i = 0; i < n; i++) {
        a[i] = rand() % 1000; // размах значений случайных величин 0 - 1000

    }

    int counter = 0;

    omp_set_num_threads(8);
    #pragma omp parallel for
    for (int i = 0; i < n; i++) {
        if (a[i] % 3 == 0) {
            #pragma omp atomic
            // увеличение счетчика происходит атомарно,
            // то есть разные потоки не могут одновременно влиять на значение переменной из-за гонки
            counter++;
        }
    }
    printf("counter = %d;\n", counter);


    return 0;
}

//output:
//min = 1; max = 946