#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <cmath>
//Написать программу, в которой, используя возможности OpenMP найти все простые числа в
//заданном с клавиатуры диапазоне. Количество потоков определить самостоятельно.
//Результат выдать на экран.

bool isPrime(int elem) {
    if (elem < 2) {
        return false;
    }
    int x = (int)sqrt(elem) + 1;
    for (int i = 2; i < x; i++) {
        if (elem % i == 0) {
            return false;
        }
    }
    return true;
}

int main() {
    int begin;
    int end;

    scanf("%d", &begin);
    scanf("%d", &end);

    int n = end - begin;

    if (n <= 0) {
        exit(1);
    }

    int a[n];
    omp_set_num_threads(8);

    double start = omp_get_wtime();
    for (int l = 0; l < 100; l++) {
        int j = begin;
        for (int i = 0; i < n; i++) {
            a[i] = j;
            j++;
        }

        #pragma omp parallel for
        for (int i = 0; i < n; i++) {
            if(!isPrime(a[i])) {
                a[i] = 0;
            }
        }
    }


    double finish = omp_get_wtime();
    printf("average time = %f\n", (finish - start) / 100);

    for (int i = 0; i < n; i++) {
        if (a[i] != 0) {
            printf("%d, ", a[i]);
        }
    }
    printf("\n");

    return 0;
}


// 1 блок через 2 недели
// 2 блок через 3 недели
// average time = 0.001595