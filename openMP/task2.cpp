#include <stdio.h>
#include <omp.h>
// Написать программу, в которой определить две параллельные области, выполнение которых
//зависит от условного оператора #pragma omp parallel if(...), если заданное значение числа
//нитей больше 1, параллельная область выполняется, иначе не выполняется. Число нитей
//перед первой областью задать равным 3, перед второй – равным 1. Внутри параллельных
//областей определить количество нитей и номер каждой нити, результат выдать на экран.
int main()
{
    #pragma omp parallel num_threads(3) if (omp_get_max_threads() > 1)
    {
        printf("Thread id = %d; Number od threads = %d; \n", omp_get_thread_num(), omp_get_num_threads());
    }

    #pragma omp parallel num_threads(1) if (omp_get_max_threads() > 1)
    {
        printf("Thread id = %d; Number od threads = %d; \n", omp_get_thread_num(), omp_get_num_threads());
    }
    // вторая прагма выполняется последовательно (1 раз), так как условие не выполняется
    return 0;
}

// output:
// Thread id = 1; Number od threads = 3;
//Thread id = 2; Number od threads = 3;
//Thread id = 0; Number od threads = 3;
//Thread id = 0; Number od threads = 1;
