#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
//Написать программу, в которой, используя возможности OpenMP найти все простые числа в
//заданном с клавиатуры диапазоне. Количество потоков определить самостоятельно.
//Результат выдать на экран.

int main() {
    int begin = 1;
    int end = 10000;

    scanf("%d", &begin);
    scanf("%d", &end);

    int n = end - begin;

    if (n <= 0) {
        exit(1);
    }

    int a[end];
    omp_set_num_threads(8);

    double start = omp_get_wtime();
    for (int l = 0; l < 100; l++) {
        for (int i = 0; i < end; i++) {
            a[i] = i;
        }
        // используем решето Эратосфена, расширим интервал до [0; end). Непростые числа будем маркировать нулями.
        a[1] = 0; // 1 непростое число
        #pragma omp parallel for
        for (int i = 2; i < end; i++) {
            if (a[i] > 1) {
                for (int k = 2 * i; k < n; k += i) {
                    a[k] = 0;
                }
            }
        }
    }
    


    double finish = omp_get_wtime();
    printf("average time = %f\n", (finish - start) / 100);

    for (int i = 0; i < end; i++) {
        if (a[i] != 0 && i >= begin) {
            printf("%d, ", a[i]);
        }
    }
    printf("\n");

    return 0;
}

//input 2, 20
//output: 2, 3, 5, 7, 11, 13, 17, 18, 19,
//0.000293