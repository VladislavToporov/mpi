#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
// Написать программу, в которой объявить и присвоить начальные значения целочисленным
//массивам a[10] и b[10]. Используя конструкцию parallel for и reduction вычислить средние
//арифметические значения элементов массивов a и b, сравнить полученные значения,
//результат выдать на экран.

int main() {
    int n = 10;
    int a[]  = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int b[]  = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    int sumA = 0;
    int sumB = 0;

        #pragma omp parallel for reduction(+:sumA)
        // reduction аккумулирует частные суммы, а затем сливает эти значение в одно в главном потоке
        for (int i = 0; i < n; i++) {
            sumA += a[i];
        }

        #pragma omp parallel for reduction(+:sumB)
        for (int i = 0; i < n; i++) {
            sumB += b[i];
        }
    double avgA = (double)sumA / n;
    double avgB = (double)sumB / n;
    printf("average of a = %f;\n", avgA);
    printf("average of b = %f;\n", avgB);
    if (avgA == avgB) {
        printf("equals");
    }
    else {
        printf("is not equals\n");
    }
    return 0;
}

//output:
//average of a = 4.500000;
//average of b = 4.500000;
//equals