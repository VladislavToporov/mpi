#include <stdio.h>
#include <omp.h>

// Написать программу, в которой объявить и присвоить начальные значения целочисленным
//массивам a[10] и b[10], определить параллельную область, количество нитей задать равным
//2, выделить код для основной (номер 0) и нити с номером 1. Основная нить должна
//выполнять поиск min значения элементов массива a, нить с номером 1 - поиск max значения
//элементов массива b. Результат выдать на экран.

int max(int a[], int n) {
    int max = a[0];
    for (int i = 0; i < n; i++) {
        if (a[i] > max) {
            max = a[i];
        }
    }
    return max;
}

int min(int a[], int n) {
    int min = a[0];
    for (int i = 0; i < n; i++) {
        if (a[i] < min) {
            min = a[i];
        }
    }
    return min;
}

int main() {
    int n = 10;
    int a[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int b[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};


    #pragma omp parallel num_threads(2)
    {
        {
            if (omp_get_thread_num() == 0)
            {
                printf("Thread id = %d; min = %d\n", omp_get_thread_num(), min(a, n));
            }
            else
            {
                printf("Thread id = %d; max = %d\n", omp_get_thread_num(), max(b, n));
            }
        }

    }
    return 0;
}

// output:
//Thread id = 1; max = 9
//Thread id = 0; min = 0