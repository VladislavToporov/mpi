#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
// Используя возможности OpenMP, написать программу умножения матрицы на вектор.
//Сравнить время выполнения последовательной и параллельных программ.

int main() {
    int n = 4;
    int a[n][n], b[n], c[n];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            a[i][j] = 1;
        }
    }
    for (int i = 0; i < n; i++) {
        b[i] = i;
    }

    double begin = omp_get_wtime();
    for (int i = 0; i < n; i++) {
        c[i] = 0;
        for (int j = 0; j < n; j++) {
            c[i] += a[i][j] * b[j];
        }
    }

    double end = omp_get_wtime();
    printf("seq time = %f;\n", end - begin);

    for (int i = 0; i < n; i++ ) {
       c[i] = 0;
    }

    begin = omp_get_wtime();
        #pragma omp parallel for collapse(2) // распределение итераций будет происходить
        // с учетом глубины вложенности цикла = 2
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                #pragma omp atomic
                c[i] += a[i][j] * b[j];
            }
        }
    end = omp_get_wtime();
    printf("par time = %f;\n", end - begin);

    for (int i = 0; i < n; i++ ) {
        printf("%d, ", c[i]);
    }
    return 0;
}
