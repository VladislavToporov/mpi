#include <stdio.h>
#include <omp.h>

#define THREADS_NUMBER 8

//Модифицируйте задачу 1 так, что бы потоки распечатывали свои идентификаторы в
//обратном порядке. Существует как минимум 5 способов решения. Постарайтесь найти как
//можно больше.

int main()
{
    int current = 7;
    omp_set_num_threads(8);
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            while(current != omp_get_thread_num()) {
            }
            printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
            current--;

        }
        #pragma omp section
        {
            while(current != omp_get_thread_num()) {
            }
            printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
            current--;

        }
        #pragma omp section
        {
            while(current != omp_get_thread_num()) {
            }
            printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
            current--;

        }
        #pragma omp section
        {
            while(current != omp_get_thread_num()) {
            }
            printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
            current--;

        }
        #pragma omp section
        {
            while(current != omp_get_thread_num()) {
            }
            printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
            current--;

        }
        #pragma omp section
        {
            while(current != omp_get_thread_num()) {
            }
            printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
            current--;

        }
        #pragma omp section
        {
            while(current != omp_get_thread_num()) {
            }
            printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
            current--;

        }
        #pragma omp section
        {
            while(current != omp_get_thread_num()) {
            }
            printf("Thread id = %d; Number od threads = %d; Hello World\n", omp_get_thread_num(), THREADS_NUMBER);
            current--;
        }
    }

    return 0;
}


