#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

//Написать параллельную программу возведения числа 210 в квадрат без операции
//умножения.
//Пояснение: Квадрат натурального числа N равен сумме первых N нечетных чисел. Например,
//32 = 9 это 1+3+5=9; 52 = 25 это 1+3+5+7+9=25;

int main() {
    int number = 33;
    int res = 0;
    omp_set_num_threads(8);
    #pragma omp parallel for reduction(+:res)
    // reduction аккумулирует частные суммы, а затем сливает эти значение в одно в главном потоке
    for (int i = 1; i < number * 2; i += 2) {
        res += i;
    }
    printf("number = %d;\n", res);

    return 0;
}

//output:
//number = 1089;
