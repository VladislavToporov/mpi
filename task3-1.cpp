#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>

int main(int argc, char** argv)
{
    int size, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    const int N = 12;

    if (N % size != 0) {
        printf("Please run with (N % size == 0) configuration.\n");
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }
    int send_count = int (N / size);
    int recv_count = send_count;

    int send_buf[N];
    if (rank == 0) {
        srand(time(NULL));
        int sum = 0;
        for (int i = 0; i < N; i++) {
            send_buf[i] = rand() % 10;
            sum += send_buf[i] > 0 ? send_buf[i] : -send_buf[i];
        }
        printf("seq sum: %d\n", sum);

    }
    int recv_buff[send_count];
    int sum_i = 0, sum = 0;

    MPI_Scatter(send_buf, send_count, MPI_INT, recv_buff, recv_count, MPI_INT, 0, MPI_COMM_WORLD);

    for (int i = 0; i < send_count; i++) {
        sum_i += recv_buff[i] > 0 ? recv_buff[i] : -recv_buff[i];
    }

    MPI_Reduce(&sum_i, &sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0){
        printf("par sum: %d\n", sum);
    }

    MPI_Finalize();
    return 0;
}