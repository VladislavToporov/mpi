#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>

struct MyType{
    int row1[8];
    int row2[8];
} ;


int main(int argc, char **argv) {
    int n = 8;
    int rows[n][n];
    struct MyType myType;
    int rank, size;

    MPI_Status mystatus;

    MPI_Init(&argc, &argv);

    int blocklengths[2] = { n, n };
    MPI_Datatype types[2] = { MPI_INT, MPI_INT };
    MPI_Aint disp[2];

    MPI_Datatype newtype;

    disp[0] = offsetof(struct MyType, row1);
    disp[1] = offsetof(struct MyType, row2);

    MPI_Type_create_struct(2, blocklengths, disp, types, &newtype);
    MPI_Type_commit(&newtype);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size != 5) {
        printf("Please run with %d processes.\n", 4);
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }

    if (rank == 4) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                rows[i][j] = i;
            }
        }

        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < n; j++) {
                myType.row1[j] = rows[i][j];
                myType.row2[j] = rows[i + 4][j];
            }
            MPI_Send(&myType, n * 2, newtype, i, 0, MPI_COMM_WORLD);
        }
    }
    else {
        MPI_Status status;
        MPI_Recv(&myType, n * 2, newtype, 4, 0, MPI_COMM_WORLD, &status);
        int rows_loc[2][n];

        for (int i = 0; i < n; i++) {
            rows_loc[0][i] = myType.row1[i];
            rows_loc[1][i] = myType.row2[i];
        }

        printf("proc %d \n", rank);

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                printf("%d, ", rows_loc[i][j]);
            }
            printf("\n");
        }
    }
    MPI_Type_free(&newtype);
    MPI_Finalize();
    return 0;
}