#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

void pingpong(int rank, int m, int *message0, int *message1) {
    double t1, t2;

    MPI_Status status;
    int *buf0 = (int *) malloc(sizeof(int) * m);
    int *buf1 = (int *) malloc(sizeof(int) * m);
    int amount = 20;

    t1 = MPI_Wtime();
    for (int k = 0; k < amount; k++) {
        if (rank == 0) {
            MPI_Send(message0, m, MPI_INT, 1, 0, MPI_COMM_WORLD);
            MPI_Recv(buf1, m, MPI_INT, 1, 1, MPI_COMM_WORLD, &status);
            MPI_Send(buf1, m, MPI_INT, 1, 0, MPI_COMM_WORLD);

        } else if (rank == 1) {
            MPI_Recv(buf0, m, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
            MPI_Send(buf0, m, MPI_INT, 0, 1, MPI_COMM_WORLD);
            MPI_Send(message1, m, MPI_INT, 0, 1, MPI_COMM_WORLD);

        }
    }
    t2 = MPI_Wtime();
    free(buf0);
    free(buf1);
    if (rank == 0) {
        printf("%f\n", (t2 - t1) * 1000000 / amount, m);
        fflush(stdout);
    }
}


void pingping(int rank, int m, int *message0, int *message1) {
    double t1, t2;
    MPI_Status status;
    int *buf0 = (int *) malloc(sizeof(int) * m);
    int *buf1 = (int *) malloc(sizeof(int) * m);
    int amount = 20;
    t1 = MPI_Wtime();
    for (int k = 0; k < amount; k++) {
        if (rank == 0) {
            MPI_Send(message0, m, MPI_INT, 1, 0, MPI_COMM_WORLD);
            MPI_Recv(buf1, m, MPI_INT, 1, 1, MPI_COMM_WORLD, &status);

        } else if (rank == 1) {
            MPI_Recv(buf0, m, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
            MPI_Send(message1, m, MPI_INT, 0, 1, MPI_COMM_WORLD);
        }
    }
    t2 = MPI_Wtime();
    free(buf0);
    free(buf1);
    if (rank == 0) {
        printf("%f\n", (t2 - t1) * 1000000 / amount, m);
        fflush(stdout);
    }

}

void generate(int m, int message[m]) {
    srand(time(NULL));
    for (int i = 0; i < m; i++) {
        message[i] = rand() % 1000;
    }
}

int main(int argc, char *argv[]) {
    int size, rank;
    int n = 50, mode = 0;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size != 2) {
        printf("Please run with two processes.\n");
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }

    for (int m = n; m < 1500; m += 50) {
        int *message0 = (int *) malloc(sizeof(int) * m);
        int *message1 = (int *) malloc(sizeof(int) * m);

        generate(m, message0);
        mode == 0 ? pingpong(rank, m, message0, message1)
        : pingping(rank, m, message0, message1);
        free(message0);
        free(message1);
    }

    MPI_Finalize();
    return 0;
}