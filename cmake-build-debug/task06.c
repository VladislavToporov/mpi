#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

//Написать программу, вычисляющую элементы вектора z  по формуле zi=xi+yi. Векторы х, у задаются на нулевом процессе
// и равными блоками пересылаются остальным процессам, a, b, - заданные числа. Пересылка данных осуществляется
// функцией MPI_Send.
// Все процессы по формуле вычисляют свои элементы массива z.
// Каждый процесс отправляет на нулевой процесс подсчитанные элементы.

void printArray(int *a, int capacity) {
    for (int i = 0; i < capacity; i++) {
        printf("%d, ", a[i]);
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    int size, rank;
    int a = 2, b = 2;
    int VECTOR_LENGTH = 3;
    MPI_Status status;
    // инициализруем параллельную область
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size < 2) {
        printf("Please run with two processes.\n");
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }
    int not_fully = VECTOR_LENGTH % (size - 1) != 0 ? 1 : 0;
    int amount = size - 1;

    if (rank == 0) {
        printf("Enter a.\n");
        scanf("%d", &a);
        printf("Enter b.\n");
        scanf("%d", &b);
        printf("Enter size.\n");
        scanf("%d", &VECTOR_LENGTH);

        int x[VECTOR_LENGTH];
        int y[VECTOR_LENGTH];
        printf("Enter x[].\n");
        for (int j = 0; j < VECTOR_LENGTH; j++) {
            scanf("%d", &x[j]);
        }
        printf("Enter y[].\n");
        for (int j = 0; j < VECTOR_LENGTH; j++) {
            scanf("%d", &y[j]);
        }
        int x_cursor = 0, y_cursor = 0;
        for (int i = 1; i <= amount; i++) {
            int capacity = (int) (VECTOR_LENGTH / (size - 1));
            if (VECTOR_LENGTH % (size - 1) != 0) {
                capacity++;
            }
            if (i == amount && not_fully == 1) {
                capacity = VECTOR_LENGTH % (size - 1);
            }
            int x_sublist[capacity];
            int y_sublist[capacity];
            for (int j = 0; j < capacity; j++) {
                x_sublist[j] = x[x_cursor++];
                y_sublist[j] = y[y_cursor++];
            }

            MPI_Send(x_sublist, capacity, MPI_INT, i, 0 + i, MPI_COMM_WORLD);
//            printf("0 sent %d numbers to %d\n", capacity, i);
            MPI_Send(y_sublist, capacity, MPI_INT, i, 1 + i, MPI_COMM_WORLD);
//            printf("0 sent %d numbers to %d\n", capacity, i);
        }

        int z[VECTOR_LENGTH];
        int z_cursor = 0;
        for (int i = 1; i <= amount; i++) {
            int capacity;
            MPI_Status status;
            MPI_Probe(i, 2 + i, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_INT, &capacity);
            int *z_sublist = (int *) malloc(sizeof(int) * capacity);
            MPI_Recv(z_sublist, capacity, MPI_INT, i, 2 + i,
                     MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//            printf("0 received %d numbers \n", capacity);
            for (int j = 0; j < capacity; j++) {
                z[z_cursor++] = z_sublist[j];
            }
        }
        printArray(z, VECTOR_LENGTH);

    }
    if (rank > 0) {
        int capacity;
        MPI_Status status;
        MPI_Probe(0, 0 + rank, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &capacity);
        int z_sublist[capacity];
        int *x_sublist = (int *) malloc(sizeof(int) * capacity);
        MPI_Recv(x_sublist, capacity, MPI_INT, 0, 0 + rank,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//        printf("%d received %d numbers from 0 \n", rank, capacity);
//        printArray(x_sublist, capacity);

        MPI_Probe(0, 1 + rank, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &capacity);
        int *y_sublist = (int *) malloc(sizeof(int) * capacity);
        MPI_Recv(y_sublist, capacity, MPI_INT, 0, 1 + rank,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//        printf("%d received %d numbers from 0 \n", rank, capacity);
//        printArray(y_sublist, capacity);

        for (int i = 0; i < capacity; i++) {
            z_sublist[i] = a * x_sublist[i] + b * y_sublist[i];
        }
        MPI_Send(z_sublist, capacity, MPI_INT, 0, 2 + rank, MPI_COMM_WORLD);
//        printf("%d sent %d numbers to 0 \n", rank, capacity);
//        printArray(z_sublist, capacity);

    }
    // закрываем параллельную область
    MPI_Finalize();
    return 0;
}