#include <stdio.h>
#include <mpi.h>


//Написать mpi программу, печатающую "Hello, World!". Запустить программу на 4 процессах.

int main(int argc, char **argv)
{
    int nPr, size;
    MPI_Init(&argc, &argv);
    printf("Hello, World!\n");
    MPI_Finalize();
    return 0;
}