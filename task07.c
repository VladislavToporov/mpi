#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

void printArray(int *a, int capacity) {
    for (int i = 0; i < capacity; i++) {
        printf("%d, ", a[i]);
    }
    printf("\n");
}

int linear_transform(int a, int b, int x, int y) {
    return a * x + b * y;
}

int mult(int x, int y) {
    return x * y;
}


int main(int argc, char *argv[]) {
    int size, rank;
    int a, b, mode;
    int VECTOR_LENGTH;
    MPI_Status status;
    // инициализруем параллельную область
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size < 2) {
        printf("Please run with two and more processes.\n");
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }
    int amount = size - 1;

    if (rank == 0) {
        printf("Enter mode (0, 1, or 2).\n");
        scanf("%d", &mode);
        if (mode == 0) {
            printf("Enter a.\n");
            scanf("%d", &a);
            printf("Enter b.\n");
            scanf("%d", &b);
        }
        printf("Enter size.\n");
        scanf("%d", &VECTOR_LENGTH);
        int not_fully = VECTOR_LENGTH % (size - 1) != 0 ? 1 : 0;
        int x[VECTOR_LENGTH];
        int y[VECTOR_LENGTH];
        printf("Enter x[].\n");
        for (int j = 0; j < VECTOR_LENGTH; j++) {
            scanf("%d", &x[j]);
        }
        printf("Enter y[].\n");
        for (int j = 0; j < VECTOR_LENGTH; j++) {
            scanf("%d", &y[j]);
        }
        int x_cursor = 0, y_cursor = 0;
        for (int i = 1; i <= amount; i++) {
            int capacity = (int) (VECTOR_LENGTH / (size - 1));
            if (VECTOR_LENGTH % (size - 1) != 0) {
                capacity++;
            }
            if (i == amount && not_fully == 1) {
                capacity = VECTOR_LENGTH % (size - 1);
            }
            int x_sublist[capacity];
            int y_sublist[capacity];
            for (int j = 0; j < capacity; j++) {
                x_sublist[j] = x[x_cursor++];
                y_sublist[j] = y[y_cursor++];
            }
            MPI_Send(&mode, 1, MPI_INT, i, 10, MPI_COMM_WORLD);
            if (mode == 0) {
                MPI_Send(&a, 1, MPI_INT, i, 10, MPI_COMM_WORLD);
                MPI_Send(&b, 1, MPI_INT, i, 10, MPI_COMM_WORLD);
            }
            MPI_Send(x_sublist, capacity, MPI_INT, i, 0 + i, MPI_COMM_WORLD);
//            printf("0 sent %d numbers to %d\n", capacity, i);
            MPI_Send(y_sublist, capacity, MPI_INT, i, 1 + i, MPI_COMM_WORLD);
//            printf("0 sent %d numbers to %d\n", capacity, i);
        }

        if (mode <= 1) {
            int z[VECTOR_LENGTH];
            int z_cursor = 0;
            for (int i = 1; i <= amount; i++) {
                int capacity;
                MPI_Status status;
                MPI_Probe(i, 2 + i, MPI_COMM_WORLD, &status);
                MPI_Get_count(&status, MPI_INT, &capacity);
                int *z_sublist = (int *) malloc(sizeof(int) * capacity);
                MPI_Recv(z_sublist, capacity, MPI_INT, i, 2 + i,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//            printf("0 received %d numbers \n", capacity);
                for (int j = 0; j < capacity; j++) {
                    z[z_cursor++] = z_sublist[j];
                }
            }
            printArray(z, VECTOR_LENGTH);
        } else {
            int cursor = 0;
            for (int i = 1; i <= amount; i++) {
                int capacity;
                MPI_Status status;
                MPI_Probe(i, 2 + i, MPI_COMM_WORLD, &status);
                MPI_Get_count(&status, MPI_INT, &capacity);
                int *x_sublist = (int *) malloc(sizeof(int) * capacity);
                MPI_Recv(x_sublist, capacity, MPI_INT, i, 2 + i,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);

                MPI_Probe(i, 2 + i, MPI_COMM_WORLD, &status);
                MPI_Get_count(&status, MPI_INT, &capacity);
                int *y_sublist = (int *) malloc(sizeof(int) * capacity);
                MPI_Recv(y_sublist, capacity, MPI_INT, i, 2 + i,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);


                for (int j = 0; j < capacity; j++) {
                    x[cursor] = x_sublist[j];
                    y[cursor++] = y_sublist[j];

                }
            }
            printArray(x, VECTOR_LENGTH);
            printArray(y, VECTOR_LENGTH);
        }
    }
    if (rank > 0) {
        MPI_Status status;
        int a, b, mode;
        MPI_Recv(&mode, 1, MPI_INT, 0, 10, MPI_COMM_WORLD, &status);
        if (mode == 0) {
            MPI_Recv(&a, 1, MPI_INT, 0, 10, MPI_COMM_WORLD, &status);
            MPI_Recv(&b, 1, MPI_INT, 0, 10, MPI_COMM_WORLD, &status);


        }
//        printf("%d received %d \n", rank, mode);
        int capacity;
        MPI_Probe(0, 0 + rank, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &capacity);
        int z_sublist[capacity];
        int *x_sublist = (int *) malloc(sizeof(int) * capacity);
        MPI_Recv(x_sublist, capacity, MPI_INT, 0, 0 + rank,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//        printf("%d received %d numbers from 0 \n", rank, capacity);

        MPI_Probe(0, 1 + rank, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &capacity);
        int *y_sublist = (int *) malloc(sizeof(int) * capacity);
        MPI_Recv(y_sublist, capacity, MPI_INT, 0, 1 + rank,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//        printf("%d received %d numbers from 0 \n", rank, capacity);

        if (mode == 0) {
            for (int i = 0; i < capacity; i++) {
                z_sublist[i] = linear_transform(a, b, x_sublist[i], y_sublist[i]);
                z_sublist[i] = a * x_sublist[i] + b * y_sublist[i];
            }
            MPI_Send(z_sublist, capacity, MPI_INT, 0, 2 + rank, MPI_COMM_WORLD);
//                printf("%d sent %d numbers to 0 \n", rank, capacity);

        } else if (mode == 1) {
            for (int i = 0; i < capacity; i++) {
                z_sublist[i] = mult(x_sublist[i], y_sublist[i]);
            }
            MPI_Send(z_sublist, capacity, MPI_INT, 0, 2 + rank, MPI_COMM_WORLD);
//                printf("%d sent %d numbers to 0 \n", rank, capacity);
        } else {
            for (int i = 0; i < capacity; i++) {
                int tmp = x_sublist[i];
                x_sublist[i] = y_sublist[i];
                y_sublist[i] = tmp;
            }
            MPI_Send(x_sublist, capacity, MPI_INT, 0, 2 + rank, MPI_COMM_WORLD);
//            printf("%d sent %d numbers to 0 \n", rank, capacity);
            MPI_Send(y_sublist, capacity, MPI_INT, 0, 2 + rank, MPI_COMM_WORLD);
//            printf("%d sent %d numbers to 0 \n", rank, capacity);

        }
    }
    // закрываем параллельную область
    MPI_Finalize();
    return 0;
}