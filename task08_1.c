#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>


void printMatrix(int *z, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            printf("%d, ", z[i * n + j]);
        }
        printf("\n");
    }
    printf("\n");
}


void initRow(int *x, int *row, int n, int i) {
    for (int j = 0; j < n; j++) {
        row[j] = x[i * n + j];
    }
}

void collect(int n, int size) {
    MPI_Status status;
    int z[n * n];
    for (int i = 0; i < size; i++) {
        int *z_substring = (int *) malloc(sizeof(int) * n);
        MPI_Recv(z_substring, n, MPI_INT, i, i,
                 MPI_COMM_WORLD, &status);
//        printf("%d received %d numbers from %d \n", 0, n, i);
        for (int j = 0; j < n; j++) {
            z[i * n + j] = z_substring[j];
        }
    }
    printMatrix(z, n);
}

void calculate(int n, int rank) {
    MPI_Status status;
    int z_substring[n];
    int *row = (int *) malloc(sizeof(int) * n);
    MPI_Recv(row, n, MPI_INT, 0, rank,
             MPI_COMM_WORLD, &status);
    int *column = (int *) malloc(sizeof(int) * n);
    MPI_Recv(column, n, MPI_INT, 0, rank,
             MPI_COMM_WORLD, &status);
//    printf("%d received %d numbers from %d \n", rank, n, 0);


    for (int i = 0; i < n; i++) {
        z_substring[i] = row[i] * column[i];
    }
    printf("\n");
    MPI_Send(z_substring, n, MPI_INT, 0, rank, MPI_COMM_WORLD);
//    printf("%d sent %d numbers to %d \n", rank, n, 0);
}

void sendInitialRowAndColumn(int *x, int *y, int n, int i) {
    int rowX[n];
    // init row
    initRow(x, rowX, n, i);
    int rowY[n];
    initRow(y, rowY, n, i);

    MPI_Send(rowX, n, MPI_INT, i, i, MPI_COMM_WORLD);
//            printf("0 sent %d numbers to %d \n", n, i);

    MPI_Send(rowY, n, MPI_INT, i, i, MPI_COMM_WORLD);
//            printf("0 sent %d numbers to %d \n", n, i);

}

void initMatrix(int *x, int *y, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            x[i * n + j] = i * j;
            y[i * n + j] = i * j;
        }
    }
}

int main(int argc, char *argv[]) {
    int size, rank;
    int n = 3, mode = 0;

    MPI_Status status;
    // инициализруем параллельную область
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size != n) {
        printf("Please run with %d processes.\n", n);
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }

    if (rank == 0) {

        int x[n * n], y[n * n];
        initMatrix(x, y, n);
        printMatrix(x, n);
        printMatrix(y, n);

        for (int i = 0; i < size; i++) {
           sendInitialRowAndColumn(x, y, n, i);
        }
    }

    if (mode == 0) {
        calculate(n, rank);
    }


    if (rank == 0) {
        collect(n, size);
    }

    // закрываем параллельную область
    MPI_Finalize();
    return 0;
}