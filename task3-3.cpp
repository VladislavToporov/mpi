#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>


void init(int *send_buf, int n, int m) {
    srand(time(NULL));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            send_buf[i * n + j] = rand() % 5;
            printf("%d, ",  send_buf[i * n + j]);
        }
        printf("\n");
    }
}

void transpose(int *send_buf_t, int *send_buf, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            send_buf_t[j * m + i] = send_buf[i * n + j];
        }
    }

    int max = 0;
    for (int i = 0; i < n; i++) {
        int sum = 0;
        for (int j = 0; j < m; j++) {
            sum += send_buf_t[i * n + j] > 0 ? send_buf_t[i * n + j] : -send_buf_t[i * n + j];
        }
        if (sum > max) {
            max = sum;
        }
    }
    printf("seq: max: %d\n", max);
}

int main(int argc, char** argv)
{
    int size, rank; //число процессов, номер текущего процесса
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int n = 4;
    int m = 4;
    if (size < m) {
        printf("Please run with %d or more process.\n", m);
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }

    int send_count = n;
    int recv_count = send_count;

    int send_buf[n * m];
    int send_buf_t[n * m];
    if (rank == 0) {
        init(send_buf, n, m);
        transpose(send_buf_t, send_buf, n, m);
    }

    int recv_buff[n];
    int sum_i = 0, max = 0;

    MPI_Scatter(send_buf_t, send_count, MPI_INT, recv_buff, recv_count, MPI_INT, 0, MPI_COMM_WORLD);

    for (int i = 0; i < recv_count; i++) {
        sum_i += recv_buff[i] > 0 ? recv_buff[i] : -recv_buff[i];
    }
//    printf("loc sum for %d: %d\n", rank, sum_i);

    MPI_Reduce(&sum_i, &max, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);

    if (rank == 0){
        printf("par max: %d", max);
    }

    MPI_Finalize();
    return 0;
}