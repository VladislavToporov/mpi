#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>

void printVector(int *a, int n) {
    for (int i = 0; i < n; i++) {
        printf("%d, ",  a[i]);
    }
    printf("\n");
}

void print(int *a, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            printf("%d, ",  a[i * n + j]);
        }
        printf("\n");
    }
    printf("\n");
}

void initVector(int *send_buf, int m) {
    for (int i = 0; i < m; i++) {
        send_buf[i] = rand() % 5;
    }
    printVector(send_buf, m);
}

void init(int *send_buf, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            send_buf[i * n + j] = rand() % 5;
        }
    }
    print(send_buf, n, m);
}

int main(int argc, char** argv)
{
    int size, rank; //число процессов, номер текущего процесса
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int n = 4;
    int m = 4;

    if (n % size != 0) {
        printf("Please run with %d or more process.\n", n);
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }

    int send_count = (int)(n * n / size);
    int recv_count = send_count;

    int send_buf_1[n * m];
    int send_buf_2[m];

    if (rank == 0) {
        srand(time(NULL));
        init(send_buf_1, n, m);
        initVector(send_buf_2, m);
    }

    int recv_buff_1[recv_count];
    int recv_buff_2[recv_count];

    MPI_Bcast(send_buf_2, m, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(send_buf_1, send_count, MPI_INT, recv_buff_1, recv_count, MPI_INT, 0, MPI_COMM_WORLD);
    int buf_loc[m / size]; // 50

    int counter = 0;
        for (int i = 0; i < 50; i++) {
           for ()
            buf_loc[j] += recv_buff_1[j * m + i] * send_buf_2[j * m + i];
        }
        MPI_Gather(&buf_loc, 50, MPI_INT, &recv_buff_2, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (rank == 0){
        printVector(recv_buff_2, m);
    }

    MPI_Finalize();
    return 0;
}