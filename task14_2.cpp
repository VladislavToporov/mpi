#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>

int main(int argc, char **argv) {
    int n = 8;
    int matrix[n][n];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            matrix[i][j] = i;
        }
    }
    int odd_rows[(int) (n / 2)][n];
    int even_rows[(int) (n / 2)][n];

    int rank;
    MPI_Datatype mytype;
    MPI_Datatype my_sub_type;
    MPI_Status mystatus;

    MPI_Init(&argc, &argv);

    MPI_Type_vector((int) (n / 2), n, 2 * n, MPI_INT, &mytype);
    MPI_Type_vector((int) (n / 2), n, n, MPI_INT, &my_sub_type);

    MPI_Type_commit(&mytype);
    MPI_Type_commit(&my_sub_type);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        MPI_Send(&matrix[0][0], 1, mytype, 1, 0, MPI_COMM_WORLD);
        MPI_Send(&matrix[1][0], 1, mytype, 1, 1, MPI_COMM_WORLD);
    } else {

        MPI_Recv(&even_rows[0][0], 1, my_sub_type, 0, 0, MPI_COMM_WORLD, &mystatus);
        for (int i = 0; i < n / 2; i++) {
            printf("\n");
            for (int j = 0; j < n; j++)
                printf("%i ", even_rows[i][j]);
        }


        MPI_Recv(&odd_rows[0][0], 1, my_sub_type, 0, 1, MPI_COMM_WORLD, &mystatus);
        printf("\n");
        for (int i = 0; i < n / 2; i++) {
            printf("\n");
            for (int j = 0; j < n; j++)
                printf("%i ", odd_rows[i][j]);
        }


    }

    MPI_Finalize();
    return 0;
}