#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <time.h>

void print(int *send_buf, int N) {
    int sum = 0;
    for (int i = 0; i < N; i++) {
        printf("%d, ", send_buf[i]);
    }
    printf("\n");
}

void init(int *send_buf, int N) {
    for (int i = 0; i < N; i++) {
        send_buf[i] = rand() % 5;
    }
    print(send_buf, N);
}

void sc_mult(int *send_buf_1, int * send_buf_2, int N) {
    int sum = 0;
    for (int i = 0; i < N; i++) {
        sum += send_buf_1[i] * send_buf_2[i];
    }
    printf("seq sc mult: %d\n", sum);
}
int main(int argc, char** argv)
{
    int size, rank; //число процессов, номер текущего процесса
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    const int N = 12;
    if (N % size != 0) {
        printf("Please run with (N % size == 0) configuration.\n");
        fflush(stdout);
        MPI_Finalize();
        return 0;
    }
    int send_count = int (N / size);
    int recv_count = send_count;

    int send_buf_1[N];
    int send_buf_2[N];
    if (rank == 0) {
        srand(time(NULL));
        init(send_buf_1, N);
        init(send_buf_2, N);
        sc_mult(send_buf_1, send_buf_2, N);
    }
    int recv_buff_1[send_count];
    int recv_buff_2[send_count];
    int sum_i = 0, sum = 0;

    MPI_Scatter(send_buf_1, send_count, MPI_INT, recv_buff_1, recv_count, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(send_buf_2, send_count, MPI_INT, recv_buff_2, recv_count, MPI_INT, 0, MPI_COMM_WORLD);

    for (int i = 0; i < send_count; i++) {
        sum_i += recv_buff_1[i] * recv_buff_2[i];
    }

    MPI_Reduce(&sum_i, &sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0){
        printf("par sc mult: %d\n", sum);
    }

    MPI_Finalize();
    return 0;
}