#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

//Написать программу и запустить ее на p (= 2, 3, 4, 6, 8) процессах. На нулевом процессе объявить и задать массив из
// 12 элементов. С помощью функции MPI_Send разослать блоки массива на остальные процессы.
//Размер блока массива (12/p)+1. В результате на нулевом процессе должны быть элементы массива с 0 до 12/p,
// на первом процессе с 12/p+1 до 2×(12/p), на 3м процессе с 2×(12/p)+1 до 3×(12/p) и т.д.
// Вывести элементы массива на экран на каждом процессе. Обратите внимание, что не на все процессы хватит элементов.

void printArray(int *a, int capacity) {
    for (int i = 0; i < capacity; i++) {
        printf("%d, ", a[i]);
    }
    printf("\n");
}

int main(int argc, char *argv[]) {
    int size, rank;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size < 2) {
        printf("Please run with two and more processes.\n");
        fflush(stdout);
        MPI_Finalize();
        return -1;
    }
    const int MAX_NUMBERS = 12;

    int length = (int) MAX_NUMBERS / size;
    if (MAX_NUMBERS % size != 0) {
        length++;
    }
    int thread_num = (int) MAX_NUMBERS / length;

    int not_fully = MAX_NUMBERS % length != 0 ? 1 : 0;

    if (not_fully) {
        thread_num++;
    }


    if (rank == 0) {
        int numbers[MAX_NUMBERS];
        printf("Enter x[]\n");

        for (int i = 0; i < MAX_NUMBERS; i++) {
            scanf("%d", &numbers[i]);
        }
        int cursor = 0;
        for (int i = 0; i < thread_num; i++) {
            int capacity = length;
            if (i == thread_num - 1 && not_fully) {
                capacity = MAX_NUMBERS % capacity;
            }
            int partition[capacity];
            for (int j = 0; j < capacity; j++) {
                partition[j] = numbers[cursor++];
            }
            MPI_Send(partition, capacity, MPI_INT, i, 0, MPI_COMM_WORLD);
//            printf("0 sent %d numbers to %d\n", capacity, i);
        }
    }
    if (rank < thread_num) {
        int capacity;
        MPI_Status s;
        MPI_Probe(0, 0, MPI_COMM_WORLD, &s);
        MPI_Get_count(&s, MPI_INT, &capacity);
        int *sublist = (int *) malloc(sizeof(int) * capacity);
        MPI_Recv(sublist, capacity, MPI_INT, 0, 0,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d \n", rank);
        printArray(sublist, capacity);
    }
    MPI_Finalize();
    return 0;
}